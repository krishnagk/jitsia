// @flow
import React, { Component } from 'react';

import { translate } from '../../base/i18n';

import ParticipantName from './preview/ParticipantName';

type Props = {

    /**
     * Meeting details from API
     */
    meeting: Object,

    /**
     * Flag signaling if a user is logged in or not.
     */
    isAnonymousUser: boolean,

    /**
     * Joins the current meeting.
     */
    joinConference: Function,

    /**
     * Process next button
     */
    setLogin: Function,

    /**
     * The name of the user that is about to join.
     */
    name: string,

    /**
     * Function for setting display name
     */
    setName: Function;
};

/**
 * Meeting info
 */
class MeetingInfo extends Component<Props> {

    /**
     * Component constructor.
     *
     * @param {*} props - Props.
     * @returns {void}.
     */
    constructor(props) {
        super(props);
    }

    /**
     * Render.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        const { isAnonymousUser, meeting, name, setName, setLogin } = this.props;

        return (
            <>
                <div>
                    { meeting && meeting.meetingId ? (
                        <>
                            <div className='fxt-content'>
                                <div className='fxt-header'>
                                    <a
                                        className='fxt-logo'
                                        href='#'>
                                        <img
                                            alt='rounddesk'
                                            src='./images/logo-rounddesk.png' />
                                    </a>
                                </div>
                                <div className='fxt-form'>
                                    <p className='text-center'>Join Meeting Room</p>
                                    <form>
                                        <div className='form-group'>
                                            <div className='fxt-transformY-50 fxt-transition-delay-1'>
                                                <input
                                                    className='form-control'
                                                    disabled={ true }
                                                    type='text'
                                                    value={ `Meeting ID:${meeting.meetingId}` } />
                                            </div>
                                        </div>
                                        <div className='form-group'>
                                            <div className='fxt-transformY-50 fxt-transition-delay-1'>
                                                <ParticipantName
                                                    isEditable={ isAnonymousUser }
                                                    joinConference={ () => { } }
                                                    setName={ setName }
                                                    value={ name } />
                                            </div>
                                        </div>
                                        <div className='form-group'>
                                            <div className='fxt-transformY-50 fxt-transition-delay-4'>
                                                <button
                                                    className='fxt-btn-fill'
                                                    disabled={ name === '' }
                                                    onClick={ setLogin }
                                                    type='button'>
                                                    Next
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </>
                    )
                        : <>
                            <div className='invalid-meeting'>
                                <div className='content'>
                                    <p>
                                        Unable to find a meeting with this request!
                                        <a href={ location.origin }>Try Again</a>
                                    </p>
                                </div>
                            </div>
                        </> }
                </div>
            </>
        );
    }
}

export default translate(MeetingInfo);
