import React, { memo } from 'react';
import { StyleSheet, Text, View, TextInput as Input } from 'react-native';


// type Props = React.ComponentProps<typeof Input> & { errorText?: string };

const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 12
    },
    input: {
        backgroundColor: '#fff',
        height: 40,
        paddingLeft: 6
    },
    error: {
        fontSize: 14,
        color: 'red',
        paddingHorizontal: 4,
        paddingTop: 4
    }
});

const TextInput = ({ errorText, ...props }) => (
    <View style={ styles.container }>
        <Input
            style={ styles.input }
            underlineColor="transparent"
            mode="outlined"
            { ...props }
        />
        { errorText ? <Text style={ styles.error }>{ errorText }</Text> : null }
    </View>
);

export default memo(TextInput);