import React, { memo } from 'react';
import { KeyboardAvoidingView, StyleSheet } from 'react-native';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        padding: 20,
        // alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#0077FF'
    }
});

const Background = ({ children }) => (
    <KeyboardAvoidingView style={ styles.container } behavior="padding">
        { children }
    </KeyboardAvoidingView>
);

export default memo(Background);