// @flow
import axios from 'axios';

import {appNavigate} from '../app/actions';
import {setRoom} from '../base/conference';
import {updateSettings} from '../base/settings';
import {showNotification} from '../notifications/actions';
import {NOTIFICATION_TYPE} from '../notifications/constants';
import {disconnect} from '../base/connection';
import {DEFAULT_API_URL, DEFAULT_SERVER_URL} from '../base/settings/constants';
//import {trackEvent} from '../toolbox/actions.web';

import {SET_SIDEBAR_VISIBLE, SET_WELCOME_PAGE_LISTS_DEFAULT_PAGE} from './actionTypes';
/*
import {
    hideLobbyScreen,
    knockingParticipantLeft,
    openLobbyScreen,
    participantIsKnockingOrUpdated,
    setLobbyModeEnabled,
    startKnocking,
    setPasswordJoinFailed
} from '../lobby/actions';
*/
import {setPrejoinPageVisibility, startConference, setWait} from '../prejoin/actions';
import {setPreferredVideoQuality} from '../video-quality/actions';

declare var interfaceConfig: Object;

if (typeof interfaceConfig === 'undefined') {
    interfaceConfig = {
        API_ENDPOINT: DEFAULT_API_URL
    };
}

/**
 * Sets the visibility of {@link WelcomePageSideBar}.
 *
 * @param {boolean} visible - If the {@code WelcomePageSideBar} is to be made
 * visible, {@code true}; otherwise, {@code false}.
 * @returns {{
 *     type: SET_SIDEBAR_VISIBLE,
 *     visible: boolean
 * }}
 */
export function setSideBarVisible(visible: boolean) {
    return {
        type: SET_SIDEBAR_VISIBLE,
        visible
    };
}

/**
 * Sets the default page index of {@link WelcomePageLists}.
 *
 * @param {number} pageIndex - The index of the default page.
 * @returns {{
 *     type: SET_WELCOME_PAGE_LISTS_DEFAULT_PAGE,
 *     pageIndex: number
 * }}
 */
export function setWelcomePageListsDefaultPage(pageIndex: number) {
    return {
        type: SET_WELCOME_PAGE_LISTS_DEFAULT_PAGE,
        pageIndex
    };
}

const fetchMeetingRequest = () => {
    return {
        type: 'FETCH_MEETING_START'
    };
};

const fetchMeetingSuccess = (meeting: Object) => {
    return {
        type: 'FETCH_MEETING_SUCCESS',
        meeting
    };
};

const fetchMeetingFailure = (error: string) => {
    return {
        type: 'FETCH_MEETING_FAILURE',
        error
    };
};

/**
 * Action used to set the visiblity of the prejoin page.
 *
 * @param {boolean} value - The value.
 * @returns {Object}
 */
export function getIntoLobby(value: boolean) {
    return {
        type: 'SET_LOBBY_STATUS',
        value
    };
}

export function cleanUp() {
    return (dispatch: Function) => {
        dispatch(updateSettings({meeting: null}));
        dispatch(updateSettings({user: null}));
    };
}

export function setVideoProfile(profile) {
    return (dispatch: Function, getState: Function) => {
        if (profile === 'Low bandwidth') {
    //        dispatch(setAudioOnly(false));
        } else if (profile === 'Low definition') {
            dispatch(setPreferredVideoQuality(180));
        } else if (profile === 'Standard definition') {
            dispatch(setPreferredVideoQuality(360));
        } else {
            dispatch(setPreferredVideoQuality(720));
        }
    };
}

/**
 * Get meeting infromation.
 *
 * @param {string} room  - Room name.
 * @param {*} token - Join token.
 * @param {*} cb - Navigation call back.
 * @returns {Function}.
 */
export function fetchMeeting(room: string, token: string, cb: Function) {
    console.log('fetchMeeting(room: string, token: string, cb: Function)' + " " + 'fetchMeeting(room: string, token: string, cb: Function)' + "room: " + room + " token: " + token + " cb: " + cb)

    return (dispatch: Function, getState: Function) => {

     //k   dispatch(fetchMeetingRequest());
     //k   dispatch(updateSettings({meeting: null}));
      //k  dispatch(updateSettings({user: null}));
        // dispatch(updateSettings({ unlocked: false }));

        const dt = new Date();
console.log('${interfaceConfig.API_ENDPOINT}',interfaceConfig.API_ENDPOINT)
        axios
            .post(`${interfaceConfig.API_ENDPOINT}/meeting-info`, {
                token,
                meetingOrRoomId: room,
                timeZoneOffset: dt.getTimezoneOffset()
            })
            .then(res => {
                 console.log('Response from Server:');
                 console.log(JSON.stringify(res.data));
                const meeting = res.data;
                const user = meeting.user;

            //k    dispatch(fetchMeetingSuccess(meeting));
/* k
                if (meeting.isStarted) {
                    dispatch(setWait(false));
                }
                 */
/* k
*/
                if (user) {
                    // vd
                    /*k
                    if (typeof user.videoProfile !== 'undefined') {
                        dispatch(setVideoProfile(user.videoProfile));
                    }
                    */
                    if (
                        typeof user.rejectedByHost !== 'undefined' &&
                        user.rejectedByHost !== null &&
                        user.rejectedByHost
                    ) {
                        dispatch(
                            showNotification(
                                {
                                    title: 'Host declined your join request',
                                    appearance: NOTIFICATION_TYPE.ERROR
                                },
                                50000
                            )
                        );
                        dispatch(disconnect(true));
                        dispatch(cleanUp());
          //              const uri = `${location.protocol}//${location.host}`;
          //              location.href = uri;
                    } else {
                        dispatch(updateSettings({user}));
                        // dispatch(updateSettings({ displayName: user.name }));

                        if (
                            meeting.lockRoom &&
                            user.waitingLobby &&
                            meeting.userType !== 'host' &&
                            meeting.isStarted
                        ) {
                            console.log('1111111111111111111111111111111111111111111111')
                            // add to waiting list
                            dispatch(getIntoLobby(true));
                //            dispatch(trackEvent('WAITING_ON_LOBBY'));
                            dispatch(
                                showNotification(
                                    {
                                        title: 'Added to waiting lobby.',
                                        appearance: NOTIFICATION_TYPE.INFO
                                    },
                                    5000
                                )
                            );
                        } else if (
                            meeting.lockRoom &&
                            !user.waitingLobby &&
                            meeting.userType !== 'host' &&
                            !user.rejectedByHost
                        ) {
                            console.log('222222222222222222222222222222222222222222222222')
                            dispatch(updateSettings({meeting})).then(() => {
                                dispatch(getIntoLobby(false));
                                dispatch(setPrejoinPageVisibility(false));
                                dispatch(setRoom(`${meeting.roomId}`));
                                dispatch(startConference());
                            });
                        } else if (user.rejectedByHost) {
                            console.log('3333333333333333333333333333333333333333333333333')
                //            dispatch(trackEvent('DECLINED_FROM_LOBBY'));
                            dispatch(appNavigate(undefined));
                            dispatch(updateSettings({meeting: null}));
                            dispatch(updateSettings({user: null}));
                        } else {
                            console.log('4444444444444444444444444444444444444444444444')
                            sessionStorage.removeItem('lobby');
                            dispatch(getIntoLobby(false));

                            const next = getState()['features/prejoin'].next;
console.log('next', next)
                            if (
                                !meeting.lockRoom &&
                                !user.waitingLobby &&
                                meeting.isStarted &&
                        //k        next &&
                                !user.rejectedByHost
                            ) {
                                console.log('555555555555555555555555555555555555555555555555')
                                //k dispatch(updateSettings({meeting})).then(() => {
                                //k    dispatch(getIntoLobby(false));
                                //k     dispatch(setPrejoinPageVisibility(false));
                                    dispatch(setRoom(`${meeting.roomId}`));
                                    dispatch(startConference());
                                //k});
                            } else if (user.rejectedByHost) {
                                console.log('6666666666666666666666666666666666666666666666666666')
                    //            dispatch(trackEvent('DECLINED_FROM_LOBBY'));
                                dispatch(appNavigate(undefined));
                                dispatch(updateSettings({meeting: null}));
                                dispatch(updateSettings({user: null}));
                            }
                        }
                    }
                }
/*
                */
/*k
                dispatch(updateSettings({meeting})).then(() => {
                    const uri = `${location.protocol}//${location.host}/${meeting.roomId}/${meeting.meetingId}`;

                    // if (user) {
                    //     uri += `?join=${user.code}`;
                    // }

                    dispatch(setRoom(`${meeting.roomId}`));
                    if (cb) {
                        //if (location.pathname !== `/${meeting.roomId}/${meeting.meetingId}`) {
                            console.log('In   dispatch(appNavigate(uri)).then(cb, cb);   dispatch(appNavigate(uri)).then(cb, cb);', cb)
                        dispatch(appNavigate(uri)).then(cb, cb);
                        // }
                    }
                });
                */
            })
            .catch(err => {
                dispatch(updateSettings({meeting: {error: true}}));
                let message = err.message;

                if (err.response) {
                    const data = err.response.data;

                    if (data && typeof data.message === 'string' && data.message !== '') {
                        message = data.message;
                    }
                }

                dispatch(
                    showNotification(
                        {
                            title: message,
                            appearance: NOTIFICATION_TYPE.ERROR
                        },
                        50000
                    )
                );
                dispatch(fetchMeetingFailure(message));
                dispatch(updateSettings({meeting: null}));
            });
    };
}

export function endMeeting(
    meetingId: string,
    data: Object,
    from: string,
    appNavigate: Function,
    disconnect: Function
) {
    //data.participants = JSON.decycle(data.participants); //.filter((item, index) => {

    //     let picked = (({ unlocked, network, name, local, joinTime, id, email, dominantSpeaker, connectionStatus, code, browser }) => ({ unlocked, network, name, local, joinTime, id, email, dominantSpeaker, connectionStatus, code, browser }))(item);

    //     return picked;
    // });

    const temp = [];

    data.participants.forEach(element => {
        let picked = (({
            unlocked,
            network,
            name,
            local,
            joinTime,
            id,
            email,
            dominantSpeaker,
            connectionStatus,
            code,
            browser
        }) => ({
            unlocked,
            network,
            name,
            local,
            joinTime,
            id,
            email,
            dominantSpeaker,
            connectionStatus,
            code,
            browser
        }))(element);
        temp.push(picked);
    });

    data.participants = temp;

    return (dispatch: Dispatch<any>, getState: Function) => {
        const user =
            getState()['features/base/settings'].user || getState()['features/welcome'].user;
        console.log('ending...for api');
        console.log('ending...for api', data);
        axios
            .post(`${interfaceConfig.API_ENDPOINT}/end-meeting`, {
                meetingId,
                data,
                token: typeof user !== 'undefined' ? user.code : null
            })
            .then(res => {
                console.log('called api with:');

                console.log('Response from API:', res);
                if (from === 'ReactNative') {
                    dispatch(appNavigate(undefined));
                } else {
                    dispatch(disconnect(true));
                }
                dispatch(cleanUp());
            })
            .catch(err => {
                console.log('Error: ', err);
                if (from === 'ReactNative') {
                    dispatch(appNavigate(undefined));
                } else {
                    dispatch(disconnect(true));
                }
                dispatch(cleanUp());
            });
    };
}
